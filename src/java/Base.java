package java;

public class Base {

	public static void main(String[] args) {

		boolean flag = true;
		char c = 'A';
		byte b = -100;
		short s = 24;
		int i = 24;
		long l = 890L;
		float f = 3.1415F;
		double d = 2.1828d;

		System.out.println("flag = " + flag);
		System.out.println("c = " + c);
		System.out.println("b = " + b);
		System.out.println("b.toBinaryString = " + Integer.toBinaryString(b));
		System.out.println("s = " + s);
		System.out.println("i = " + i);
		System.out.println("l = " + l);
		System.out.println("f = " + f);
		System.out.println("d = " + d);
	}

}
